using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet_Controller : MonoBehaviour
{
    [SerializeField] float force;
    public bool shootRight;
    private Rigidbody2D rb;
    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (shootRight) rb.velocity = transform.right * force;
        else
        {
            GetComponent<SpriteRenderer>().flipX= true;
            rb.velocity = -transform.right * force;
        }

        Destroy(gameObject, 5);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemie"))
        {
            GameObject contact = collision.gameObject;
            Instantiate(contact.GetComponent<Controller_Drop>().Drop(), contact.transform.position, Quaternion.Euler(new Vector3(0,0,90)));
            Destroy(contact);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Dead");
        }
    }
}
