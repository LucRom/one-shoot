using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Controller_DetectionZone : MonoBehaviour
{
    [SerializeField] UnityEvent InZone;
    [SerializeField] UnityEvent OutZone;
    [SerializeField] GameObject triggerObject;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == triggerObject) InZone.Invoke();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject == triggerObject) OutZone.Invoke();
    }
}
