using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Drop : MonoBehaviour
{
    [SerializeField] List<GameObject> droplist = new List<GameObject>();
    public GameObject Drop()
    {
        GameObject selected;
        selected = droplist[UnityEngine.Random.Range(0, droplist.Count)];
        return selected;
    }
}
