using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Controller_HUD : MonoBehaviour
{
    private Player_Aim player;
    [SerializeField] TMP_Text ammoTxt;
    void Start()
    {
        player = GameObject.Find("Pivote").GetComponent<Player_Aim>();
        UpdateText();
        Controller_Events.updateTexts += UpdateText;
    }
    private void UpdateText()
    {
        ammoTxt.text = player.ammo.ToString();
    }
}
