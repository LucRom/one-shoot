using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_JumpableFloor : MonoBehaviour
{
    
    public void TurnJumpable()
    {
        GetComponent<SpriteRenderer>().color = Color.green;
        gameObject.layer = 3;
    }

    public void TurnNotJumpable()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        gameObject.layer = 0;
    }
}
