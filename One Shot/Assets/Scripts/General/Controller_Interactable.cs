using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Controller_Interactable : MonoBehaviour
{
    private enum Actions { disableTurret, turnOff, turnOnOff}
    private Animator animator;
    [SerializeField] bool activated;
    [SerializeField] Actions action;
    [SerializeField] UnityEvent Trigger;
    [SerializeField] UnityEvent Trigger2;

    private void Start()
    {
        activated = true;
        animator = GetComponent<Animator>();
    }
    public void Interaction()
    {
        switch (action)
        {
            case Actions.disableTurret:
                activated = false;
                TriggerEvent();
                break;
            case Actions.turnOff:
                activated = false;
                TriggerEvent();
                break;
            case Actions.turnOnOff:
                activated = !activated;
                AlternateTriggerEvent();
                break;
        }
    }
    private void TriggerEvent()
    {
        Trigger.Invoke();
        TriggerAnimation();
    }
    private void AlternateTriggerEvent()
    {
        if(activated)Trigger.Invoke();
        else Trigger2.Invoke();
        TriggerAnimation();
    }
    private void TriggerAnimation()
    {
        if (activated) animator.Play("Switch-TurnOn");
        else animator.Play("Switch-TurnOff");
    }
}
