using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Events : MonoBehaviour
{
    public delegate void HudUptade();
    public static event HudUptade updateTexts;

    public void TriggerEvent(string eventTipe)
    {
        switch (eventTipe)
        {
            case "HUD":
              updateTexts?.Invoke();
                break;
        }
    }
}
