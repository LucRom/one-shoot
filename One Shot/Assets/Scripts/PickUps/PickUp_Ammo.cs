using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Ammo : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            collision.GetComponentInChildren<Player_Aim>().ammo++;
            GameObject.Find("Game Manager").GetComponent<Controller_Events>().TriggerEvent("HUD");
            Destroy(gameObject);
        }
    }
}
