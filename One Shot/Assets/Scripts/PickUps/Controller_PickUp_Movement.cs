using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PickUp_Movement : MonoBehaviour
{
    [SerializeField] float time;
    [SerializeField] float amp;
    private Vector2 initialPosition;
    private void Start()
    {
        initialPosition = transform.position;
    }
    void Update()
    {
        transform.position = new Vector3(initialPosition.x, Mathf.Sin(Time.time * time) * amp + initialPosition.y,0);
    }
}
