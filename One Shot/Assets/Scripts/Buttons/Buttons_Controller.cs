using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

public class Buttons_Controller : MonoBehaviour
{
    [SerializeField] UnityEvent Active;
    [SerializeField] UnityEvent Inactive;
    private enum state { shoot, stay, timer }
    [SerializeField] state function;
    [SerializeField] float cooldown;
    [SerializeField] bool loop;
    private float timer;
    private bool startTimer;

    private void Start()
    {
        startTimer = false;
    }
    private void Update()
    {
        if (startTimer) Timer();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && function == state.stay)
        {
            ChangeColor(Color.green);
            Active.Invoke();
        }
        if (collision.gameObject.CompareTag("Player") && function == state.timer)
        {
            ChangeColor(Color.green);
            Inactive.Invoke();
            timer = cooldown;
            startTimer = true;
        }
        if (collision.gameObject.CompareTag("Bullet") && function == state.shoot)
        {
            ChangeColor(Color.green);
            Active.Invoke();
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && function == state.stay)
        {
            ChangeColor(Color.red);
            Inactive.Invoke();
        }
    }
    private void Timer()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            Active.Invoke();
            if (loop) timer = cooldown;
            else
            {
                ChangeColor(Color.red);
                startTimer= false;
            }
        }
    }
    private void ChangeColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }
}
