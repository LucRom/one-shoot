using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemie_Aim : MonoBehaviour
{
    private Transform target;
    private Vector2 distance;
    private float rotation;
    public bool IsLookingRight { get; private set;}
    public bool PlayerIsInRange { get; private set; }
    void Start()
    {
        target = GameObject.Find("Player").transform;
    }
    void Update()
    {
        if(PlayerIsInRange)
        {
            distance = target.position - transform.position;
            distance.Normalize();
            rotation = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, rotation);
        }
        if (rotation < -90 || rotation > 90)
        {
            Flip(true, -1, 180, 180, rotation);
        }
        else
        {
            Flip(false, 1, 0, 0, rotation);
        }
    }
    
    public void Flip(bool flip, int scaleNumber, float rotX, float rotY, float rotZ)
    {
        transform.parent.GetComponent<SpriteRenderer>().flipX = flip;
        IsLookingRight= !flip;
        Vector3 localScale = transform.Find("Arm").localScale;
        localScale.x = scaleNumber;
        transform.Find("Arm").transform.localScale = localScale;
        transform.localRotation = Quaternion.Euler(rotX, rotY, rotZ);
    }
    public void LookPlayer(bool pPlayerIsInRange)
    {
        PlayerIsInRange = pPlayerIsInRange;
    }
}
