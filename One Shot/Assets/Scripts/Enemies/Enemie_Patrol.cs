using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemie_Patrol : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Transform groundCheck;
    private bool isLookingRight;
    private Rigidbody2D rb;
    private Enemie_Aim aim;
    [Header("Aniamtion")]
    private Animator animator;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        aim = transform.Find("Pivote Enemie").GetComponent<Enemie_Aim>();
        isLookingRight = true;
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (aim.PlayerIsInRange == false)
        {
            animator.Play("Enemie-Walk");
            RaycastHit2D hitD = Physics2D.Raycast(groundCheck.position, Vector2.down, 0.1f);
            RaycastHit2D hitL = Physics2D.Raycast(transform.position - new Vector3(1, 0, 0), Vector2.left, 0.1f);
            RaycastHit2D hitR = Physics2D.Raycast(transform.position + new Vector3(1, 0, 0), Vector2.right, 0.1f);
            if (hitD.collider == null || hitL.collider != null || hitR.collider != null) isLookingRight = !isLookingRight;
            if (isLookingRight)
            {
                rb.velocity = new Vector2(speed, 0);
                aim.Flip(false, 1, 0, 0, 0);
            }
            else
            {
                rb.velocity = new Vector2(-speed, 0);
                aim.Flip(true, -1, 180, 180, 180);
            }
        }
        else animator.Play("Enemie-Idle");
    }
}
