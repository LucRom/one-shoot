using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemie_Turret : MonoBehaviour
{
    private GameObject detectionZone;
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        detectionZone = transform.Find("DetectionZone").gameObject;
    }
    public void DisableTurretTimer(float timer)
    {
        DisableTurret();
        Invoke("EnableTurret", timer);
    }
    public void DisableTurret()
    {
        detectionZone.SetActive(false);
        animator.Play("Turret-TurnOff");
    }
    public void EnableTurret()
    {
        detectionZone.SetActive(true);
        animator.Play("Turret-TurnOn");
    }
    public void TriggerAttackAnimation(bool playerDetected)
    {
        if (playerDetected) animator.Play("Turret-Attack");
        else animator.Play("Turret-Idle");
    }
}
