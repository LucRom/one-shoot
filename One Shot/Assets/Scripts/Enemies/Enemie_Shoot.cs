using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemie_Shoot : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] Transform bulletSpawn;
    [SerializeField] GameObject bullet;
    [SerializeField] float cooldown;
    [Header("Turret?")]
    [SerializeField] bool turret;
    [SerializeField] bool turretIsLookingRight;
    private bool canShoot;
    void Start()
    {
        if(turret == false)InvokeRepeating("Shoot", 1, cooldown);
    }
    public void Shoot()
    {
        if(canShoot)
        {
            GameObject newBullet;
            newBullet = Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.rotation);
            Bullet_Controller bulletProperties = newBullet.GetComponent<Bullet_Controller>();
            if (turret)
            {
                if(turretIsLookingRight) bulletProperties.shootRight = true;
                else bulletProperties.shootRight = false;
            }
            else
            {
                if (GetComponent<Enemie_Aim>().IsLookingRight) bulletProperties.shootRight = true;
                else bulletProperties.shootRight = false;
            }
        }
    }
    public void CanShoot(bool pCanShoot)
    {
        canShoot = pCanShoot;
    }
}
