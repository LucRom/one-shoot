using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player_Interaction : MonoBehaviour
{
    private GameObject target;
    private bool canInteract;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Interactable"))
        {
            target = collision.gameObject;
            canInteract= true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Interactable"))
        {
            target = null;
            canInteract = false;
        }
    }
    private void Interact()
    {
        target.GetComponent<Controller_Interactable>().Interaction();
    }
    public void SetInteract(InputAction.CallbackContext context)
    {
        if (context.performed && canInteract) Interact(); 
    }
}
