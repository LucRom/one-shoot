using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player_Aim : MonoBehaviour
{
    [SerializeField] Transform bulletSpawn;
    [SerializeField] GameObject bullet;
    GameObject player;
    private Vector2 aim;
    private bool isLookingRight;
    public int ammo;

    private void Start()
    {
        isLookingRight = true;
        player = this.transform.parent.gameObject;
    }
    private void Update()
    {
        Flip();
    }
    private void FixedUpdate()
    {
        Vector3 distance = Camera.main.ScreenToWorldPoint(aim) - transform.position;
        distance.Normalize();
        float rotation = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0,0,rotation);
        if (player.transform.localScale.x == 1)
        {
            transform.localRotation = Quaternion.Euler(0, 0, rotation);
        }
        else if (player.transform.localScale.x == -1)
        {
            transform.localRotation = Quaternion.Euler(180, 180, -rotation);
        }
        if (rotation < -90 || rotation > 90) isLookingRight = false;
        else isLookingRight = true;
    }
    private void Flip()
    {
        if (isLookingRight)
        {
            Vector3 localScale = player.transform.localScale;
            localScale.x = 1;
            player.transform.localScale = localScale;
        }
        else
        {
            Vector3 localScale = player.transform.localScale;
            localScale.x = -1;
            player.transform.localScale = localScale;
        }
    }
    private void Shoot()
    {
        if(ammo > 0)
        {
            ammo--;
            GameObject.Find("Game Manager").GetComponent<Controller_Events>().TriggerEvent("HUD");
            GameObject newBullet;
            newBullet = Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.rotation);
            if (isLookingRight) newBullet.GetComponent<Bullet_Controller>().shootRight = true;
            else newBullet.GetComponent<Bullet_Controller>().shootRight = false;
        }
    }
    public void SetAim(InputAction.CallbackContext context)
    {
        aim = context.ReadValue<Vector2>();
    }
    public void SetShoot(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Shoot();
        }
    }
}
