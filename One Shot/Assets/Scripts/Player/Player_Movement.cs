using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class Player_Movement : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] float movementSpeed;
    [SerializeField] float jumpForce;
    [SerializeField] float fallMultiplier;
    [SerializeField] int cantJumpsMax;
    [SerializeField] Transform groundCheckTransform;
    [SerializeField] LayerMask jumpable;
    private Vector2 movement;
    private Vector2 gravity;
    private bool canJump;
    private int jumps;
    [Header("Animacion")]
    private string currentState;
    private const string PLAYER_IDLE = "Player_Idle";
    private const string PLAYER_WALK = "Player_Walk";
    private const string PLAYER_JUMP = "Player_Jump";
    private bool NotJumpable;
    [Header("Components")]
    private Rigidbody2D rb;
    private Animator anim;
    void Start()
    {
        canJump = true;
        jumps = 0;
        gravity = new Vector2(0, -Physics.gravity.y);
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void FixedUpdate()
    {
        Movement();
        JumpMechanics();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer != 3)
        {
            NotJumpable = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        NotJumpable = false;
    }
    private bool GroundCheck()
    {
        return Physics2D.OverlapCapsule(groundCheckTransform.position, new Vector2(0.4f, 0.1f), CapsuleDirection2D.Horizontal, 0, jumpable);
    }
    private void Movement()
    {
        rb.velocity = new Vector2(movement.x * movementSpeed, rb.velocity.y);
        if (GroundCheck() || NotJumpable)
        {
            if (rb.velocity.x != 0) Changeanimations(PLAYER_WALK, false);
            else Changeanimations(PLAYER_IDLE, false);
        }
        else Changeanimations(PLAYER_JUMP, true);
    }   

    private void JumpMechanics()
    {
        if(jumps == cantJumpsMax) canJump = false;
        if (GroundCheck() && canJump == false)
        {
            canJump = true;
            jumps = 0;
        }
        if (rb.velocity.y < 0) rb.velocity -= gravity * fallMultiplier * Time.deltaTime;
    }
    private void Jump()
    {
        jumps++;
        if (canJump && jumps <= cantJumpsMax) rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        else canJump = false;
    }
    public void SetMovement(InputAction.CallbackContext context)
    {
        movement = context.ReadValue<Vector2>();
    }
    public void SetJump(InputAction.CallbackContext context)
    {
        if (context.performed) Jump();
    }
    private void Changeanimations(string state, bool repeat)
    {
        if(currentState == state && repeat == false) return;
        anim.Play(state);
        currentState = state;
    }
}
