using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors_AnimationController : MonoBehaviour
{
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void TriggerAnimation(bool open)
    {
        if (open) animator.Play("Door-Open");
        else animator.Play("Door-Close");
    }
}
