using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] List<GameObject> electricGameObjects = new List<GameObject>();
    public static bool gameOver;
    private bool electricOn;
    private void Start()
    {
        electricOn = true; 
    }
    void Update()
    {
        if (gameOver) Restart();
    }
    private void Restart()
    {
        Debug.Log("Restart");
        gameOver = false;
        player.transform.position = new Vector3 (0, -1.65f, 0);
    }
    public void SetOnOff()
    {
        electricOn = !electricOn;
        foreach (GameObject g in electricGameObjects)
        {
            g.SetActive(electricOn);
        }
    }
}
